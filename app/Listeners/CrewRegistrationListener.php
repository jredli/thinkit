<?php

namespace App\Listeners;

use App\Events\SendCrewRegistrationEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CrewRegistrationListener
{
    /**
     * Handle the event.
     *
     * @param  SendCrewRegistrationEvent  $event
     * @return void
     */
    public function handle(SendCrewRegistrationEvent $event)
    {
        return redirect()->route('send.email', [
            'userId' => $event->user->id
        ]);

    }
}

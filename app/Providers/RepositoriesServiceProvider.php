<?php

namespace App\Providers;

use App\Repositories\NotificationRepository;
use App\Repositories\NotificationRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(NotificationRepositoryInterface::class, NotificationRepository::class);
    }
}

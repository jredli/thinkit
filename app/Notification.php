<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public $table = 'notifications';

    protected $fillable = [
        'content'
    ];

    public function users() {
        return $this->belongsToMany('App\User', 'users_notifications');
    }
}

<?php

namespace App\Repositories;

use App\Notification;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class NotificationRepository implements NotificationRepositoryInterface {

    public function all() {

        return DB::table('notifications')
            ->join('users_notifications', 'notifications.id', '=', 'users_notifications.notification_id')
            ->join('users', 'users_notifications.user_id', '=', 'users.id')
            ->where('users.id', '=', Auth::user()->id)
            ->get();
    }
}

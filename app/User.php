<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable
{
    const ROLE_ADMIN = 1;
    const ROLE_CREW = 2;

    use Notifiable;

    public $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'email', 'role_id', 'ship_id', 'rank_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function role() {
        return $this->belongsTo('App\Role');
    }

    public function ship() {
        return $this->belongsTo('App\Ship');
    }

    public function rank() {
        return $this->belongsTo('App\Rank');
    }

    public function isAdmin() {
        return $this->role->name == 'admin' ? true : false;
    }

    public function notifications() {
        return $this->belongsToMany('App\Notification', 'users_notifications');
    }
}

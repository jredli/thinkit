<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ShipAdd;
use App\Ship;
use App\Rank;

class ShipController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ships = Ship::all();
        $ranks = Rank::all();
        return view('ships.home', compact('ships', 'ranks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShipAdd $request)
    {
        $ship = new Ship();

        $ship->name = $request->name;
        $ship->serial_number = $request->serial_number;

        if ($file = $request->file('profile_image')) {
            $destinationPath = 'images';
            $image = date('YmdHis') . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $image);
            $ship->image = $image;
        }

        $ship->save();

        return redirect()->route('ships.index')->with('status', 'New ship added successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

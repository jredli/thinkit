<?php

namespace App\Http\Controllers;

use App\Events\SendCrewRegistrationEvent;
use App\Http\Requests\CrewAdd;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CrewController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CrewAdd $request)
    {
        $crew = new User();

        $crew->name = $request->name;
        $crew->surname = $request->surname;
        $crew->email = $request->email;
        $crew->ship_id = $request->ship_id;
        $crew->rank_id = $request->rank;
        $crew->role_id = User::ROLE_CREW;

        $crew->save();

        return redirect()->route('send.email', [
            'userId' => $crew->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($userId)
    {
//        return view('crew.edit', compact('userId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function mail($userId)
    {
        $user = User::find($userId);

        Mail::to($user->email)->send(new \App\Mail\CrewRegistrationMail($user->id));

        return redirect()->route('ships.index')->with('status', 'Crew member added successfully!');
    }
}

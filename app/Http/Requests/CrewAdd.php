<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CrewAdd extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|alpha',
            'surname' => 'required|alpha',
            'email' => 'required|email',
            'rank' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Crew name is required!',
            'name.alpha' => 'Crew name must be alphabetic characters only!',
            'surname.required' => 'Crew surname must be alphabetic characters only!',
            'surname.alpha' => 'Crew surname must be alphabetic characters only!',
            'email.required' => 'Email is required!',
            'email.email' => 'Email must be in correct format!',
            'rank.required' => 'Rank is required!',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RankAdd extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|alpha_dash',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Rank name is required!',
            'name.alpha_dash' => 'Rank name must contain alpha-numeric characters only!',
        ];
    }
}

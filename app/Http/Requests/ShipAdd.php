<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShipAdd extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'serial_number' => 'required|digits:8',
            'profie_image' => 'required|image',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Ship name is required!',
            'name.string' => 'Ship name must be a string!',
            'serial_number.required' => 'Serial number is required!',
            'serial_number.digits' => 'Serial number must have 8 digits!',
            'profie_image.required' => 'Ship image is required!',
            'profie_image.image' => 'Wrong image format!',
        ];
    }
}

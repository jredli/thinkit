<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Mail;

Route::get('/', function () {
    if(!Auth::user()) {
        return view('auth.login');
    }
    else
        return redirect()->back();
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home')->middleware('admin');

Route::resource('ships', 'ShipController', ['only' => [
    'index', 'store', 'destroy', 'update'
]])->middleware('admin', 'auth');

Route::resource('ranks', 'RankController', ['only' => [
    'index', 'store', 'destroy', 'update'
]])->middleware('admin', 'auth');

Route::resource('crew', 'CrewController', ['only' => [
    'index', 'store', 'destroy', 'update'
]])->middleware('admin', 'auth');

Route::get('crew/{id}', 'CrewController@edit')->name('crew.edit');

Route::resource('notifications', 'NotificationController', ['only' => [
    'index', 'store', 'update', 'edit', 'show'
]])->middleware('auth');

Route::patch('save-password/{userId}', 'NotificationController@savePassword')->name('save.password');

Route::get('send-email/{userId}', 'CrewController@mail')->name('send.email');



@extends('layouts.app')

@section('content')
    @yield('content')
    <!-- Logout Modal-->
    <div class="modal fade" id="newRankModal" data-modalname="newRankModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New rank</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form class="user" action="{{ route('ranks.store') }}" method="POST">
                        @csrf

                        <div class="form-group">
                            <input id="name" type="text" class="form-control form-control-user @error('name') is-invalid @enderror" name="name" placeholder="Rank name" value="{{ old('name') }}" required>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Ranks</h1>

    @if(session('status'))
        <div class="alert card mb-4 py-3 border-bottom-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="card-body">
                {{ session('status') }}
            </div>
        </div>
    @endif

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <a href="#" data-toggle="modal" data-target="#newRankModal" data-name="newRankModal" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-plus"></i>
                    </span>
                <span class="text">New Rank</span>
            </a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($ranks as $rank)
                        <tr>
                            <td class="align-middle">{{ $rank->name }}</td>
                            <td class="align-middle">

                                <form action="{{ URL::route('ranks.destroy', $rank->id) }}" method="POST" >
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-circle btn-sm">
                                        <i class="fas fa-trash"></i>
                                    </a>
                                </form>

{{--                                <form method="UPDATE" action="{{ route('ranks.update', $rank->id) }}">--}}
{{--                                    <a href="#" class="btn btn-danger btn-circle btn-sm">--}}
{{--                                        <i class="fas fa-pen"></i>--}}
{{--                                    </a>--}}
{{--                                </form>--}}
{{--                                --}}
{{--                                <a href="#IDEMODAL" class="btn btn-warning btn-circle btn-sm">--}}
{{--                                    <i class="fas fa-pen"></i>--}}
{{--                                </a>--}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

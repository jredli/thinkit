// Display request validation errors in modal based on clicked modal
$('a.btn').click(function() {
    var modal = $(this).data('name');
    var modalOriginal = '#' + modal;
    modal = '#' + modal + ' .invalid-feedback';

    localStorage.setItem('modalName', modal);
    localStorage.setItem('modalOriginalName', modalOriginal);
});

$(window).on("load", function () {
    var modalName = localStorage.getItem('modalName');
    var modalOriginalName = localStorage.getItem('modalOriginalName');

    if ($(modalName).length){
        $(modalOriginalName).modal('show');
    }
});

// Set hidden field for +crew modal
$('.ship-id').click(function() {
    var ship_id = $(this).data('id');
    $("#newCrewModal #hiddenValue").val(ship_id);
});
